import 'dart:math';
import 'package:flutter/material.dart';

void main() {
  return runApp(
    MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.red,
        appBar: AppBar(
          title: const Text('Dicee'),
          backgroundColor: Colors.red,
        ),
        body: const DicePage(),
      ),
    ),
  );
}

class DicePage extends StatefulWidget {
  const DicePage({Key? key}) : super(key: key);

  @override
  State<DicePage> createState() => _DicePageState();
}

class _DicePageState extends State<DicePage> {
  final int numDice = 2;

  @override
  Widget build(BuildContext context) {
    List<DiceWidget> die = rollDice(numDice);

    return Center(
      child: InkWell(
        onTap: () {
          setState(() {
            die = rollDice(numDice);
          });
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: die,
        ),
      ),
    );
  }

  List<DiceWidget> rollDice(int numberOfDice) {
    List<DiceWidget> die = [];
    for (var i = 0; i < numberOfDice; i++) {
      die.add(DiceWidget(Random().nextInt(6) + 1));
    }

    return die;
  }
}

class DiceWidget extends StatelessWidget {
  final int diceNumber;

  const DiceWidget(this.diceNumber, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Image.asset('assets/dice$diceNumber.png'),
      ),
    );
  }
}
